<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Dokter extends Model
{
    public $incrementing = false;
	protected $table = 'dokter';
	protected $fillable = ['id','nama_dokter','id_poli','user_id','created_at','updated_at','deleted_at'];
}
