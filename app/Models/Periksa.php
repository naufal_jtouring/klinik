<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Periksa extends Model
{
    public $incrementing = false;
	protected $table = 'pemeriksaan';
	protected $fillable = ['id','user_id','poli_id','dokter_id','status','petugas','created_at','updated_at','deleted_at','remark'];
}
