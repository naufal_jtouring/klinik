<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Poli extends Model
{
    public $incrementing = false;
	protected $table = 'poli';
	protected $fillable = ['id','nama_poli','created_at','updated_at','deleted_at'];
}
