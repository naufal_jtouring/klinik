<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Kategory extends Model
{
    
	public $incrementing = true;
	protected $guarded = ['id'];
	protected $table = 'kategory';
	protected $fillable = ['nama_kategory','created_at','updated_at','deleted_at','type'];
}
