<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public $incrementing = false;
	protected $table = 'product';
	protected $fillable = ['id','nama_product','harga','kode_product','id_satuan','id_kategory','created_at','updated_at','deleted_at'];
}
