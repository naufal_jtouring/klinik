<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pembayaran extends Model
{
    public $incrementing = false;
	protected $table = 'pembayaran';
	protected $fillable = ['id','status_pembayaran','total','bayar','kembali','potongan','petugas','created_at','updated_at','deleted_at','id_pemeriksaan'];
}
