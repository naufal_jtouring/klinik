<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Satuan extends Model
{
    public $incrementing = true;
	protected $guarded = ['id'];
	protected $table = 'satuan';
	protected $fillable = ['nama_satuan','created_at','updated_at','deleted_at'];
}
