<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Uuids;

class DetailResep extends Model
{
    use Uuids;
	public $incrementing = false;
	protected $guarded = ['id'];
	protected $table = 'detail_resep';
	protected $fillable = ['id_resep','id_product','qty','harga','sub_total','created_at','updated_at','deleted_at'];
}
