<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Pasien extends Model
{
    
	public $incrementing = false;
	protected $table = 'pasien';
	protected $fillable = ['user_id','nama','nik','tempat_lahir','tanggal_lahir','gender','agama','alamat','no_hp','created_at','updated_at','deleted_at','id_kel'];
}
