<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use DataTables;
use Auth;
use Carbon\Carbon;
use App\Models\Dokter;
use App\Models\Poli;

class DokterController extends Controller
{
	//Poli
	public function poli(){
		return view('master.poli.list_poli');
	}

	public function ajaxListPoli(){
		$data = Poli::OrderBy('created_at','desc');

		return DataTables::of($data)
						->make(true);
	}

	public function addPoli(Request $request){
		$poli = Poli::OrderBy('created_at','desc')->first();
		
		if ($poli==null) {
			$sub = 0;
		}else{
			$last = $poli->id;
			$sub =(int) substr($last->id,4);
		}

		$new = 'POL-'.sprintf("%03s", $sub+1);

		try {
			DB::begintransaction();
			$in = array(
				'id'=>$new,
				'nama_poli'=>strtoupper(trim($request->nama))
			);

			Poli::firstOrCreate($in);

			
			DB::commit();
			$data_response = [
                            'status' => 200,
                            'output' => 'Tambah Poli Sukses . . .'
                          ];
		} catch (Exception $ex) {
			DB::rollback();
            $message = $ex->getMessage();
            ErrorHandler::db($message);
            $data_response = [
                            'status' => 422,
                            'output' => 'Tambah Poli Gagal ! ! !'
                          ];
		}

		return response()->json(['data'=>$data_response]);
	}
	//Poli
	// Dokter
    public function dokter(){
    	return view('master.dokter.list_dokter');
    }

    public function ajaxListDokter(){
    	$data = Dokter::OrderBy('created_at','desc');

    	return DataTables::of($data)
    					->addColumn('action',function($data){
    						return $data->id;
    					})
    					->editColumn('id_poli',function($data){
    						$poli = Poli::where('id',$data->id_poli)->first();

    						return $poli->nama_poli;
    					})
    					->rawColumns(['action'])->make(true);
    }

    public function ajaxGetPoli(){
    	$data = Poli::whereNull('deleted_at')->OrderBy('created_at','desc')->get();

    	return response()->json(['poli'=>$data],200);
    }

    public function ajaxGetUser(){
    	$data = DB::table('users')->whereNull('deleted_at')->OrderBy('created_at','desc')->get();

    	return response()->json(['user'=>$data],200);
    }

    public function addDokter(Request $request){
    	try {
			DB::begintransaction();
			$in = array(
				'id'=>strtoupper(trim($request->nid)),
				'nama_dokter'=>strtoupper(trim($request->nama)),
				'id_poli'=>$request->poli,
				'user_id'=>$request->userid
			);

			Dokter::firstOrCreate($in);

			
			DB::commit();
			$data_response = [
                            'status' => 200,
                            'output' => 'Tambah Dokter Sukses . . .'
                          ];
		} catch (Exception $ex) {
			DB::rollback();
            $message = $ex->getMessage();
            ErrorHandler::db($message);
            $data_response = [
                            'status' => 422,
                            'output' => 'Tambah Dokter Gagal ! ! !'
                          ];
		}

		return response()->json(['data'=>$data_response]);
    }

    // Dokter
}
