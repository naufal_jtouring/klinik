<?php

namespace App\Http\Controllers\Layanan;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use DataTables;
use Auth;
use Carbon\Carbon;
use App\Models\Resep;
use App\Models\DetailResep;

class ResepController extends Controller
{
    public function resep(){
    	$prod = DB::table('product')
    				->join('kategory','product.id_kategory','=','kategory.id')
    				->whereNull('product.deleted_at')
    				->where('kategory.type','BARANG')
    				->select('product.nama_product','product.id')
    				->orderBy('product.created_at','asc')->get();

    	return view('layanan.resep')->with('prod',$prod);
    }

    public function getProduct(Request $request){
    	$prod = DB::table('product')
    					->join('satuan','product.id_satuan','=','satuan.id')
    					->join('kategory','product.id_kategory','=','kategory.id')
    					->where('product.id',$request->id)
    					->where('kategory.type',"BARANG")
    					->whereNull('product.deleted_at')
    					->select('product.id','product.nama_product','product.harga','product.kode_product','satuan.nama_satuan')->first();

    	return response()->json(['prod'=>$prod],200);
    }

    public function addTransaksi(Request $request){
    	$resep = $request->resep;
        $noperiksa = $request->noperiksa;

    	try {
			DB::begintransaction();
			$headresep = array(
				'id_pemeriksaan'=>$noperiksa,
				'petugas'=>Auth::user()->id,
				'created_at'=>Carbon::now()
			);

			$inpem = Resep::firstOrCreate($headresep);

			if ($inpem->id) {
				foreach ($resep as $rsp) {
					$dtresp = array(
						'id_resep'=>$inpem->id,
						'id_product'=>$rsp['id_prod'],
						'qty'=>$rsp['qty'],
						'harga'=>$rsp['harga'],
						'sub_total'=>$rsp['sub_tot'],
						'created_at'=>Carbon::now()
					);
				}

				DetailResep::firstOrCreate($dtresp);
			}
			
			
			DB::commit();
			$data_response = [
                            'status' => 200,
                            'output' => 'Simpan data Sukses . . .'
                          ];
		} catch (Exception $ex) {
			DB::rollback();
            $message = $ex->getMessage();
            ErrorHandler::db($message);
            $data_response = [
                            'status' => 422,
                            'output' => 'Simpan data Gagal ! ! !'
                          ];
		}

		return response()->json(['data'=>$data_response]);
    }

    private function kodeTrans(){
    	$now = Carbon::now()->format('Y-m-d');
    	$trans = Pembayaran::whereDate('created_at',$now)->orderBy('created_at','desc')->first();

    	if ($trans==null) {
    		$last = 0;
    	}else{
    		$last = (int) substr($trans->id,8);
    	}

    	$new = $now.sprintf("%06s", $last+1);

    	return $new;
    }

    public function ajaxGetPeriksa(Request $request){
        $id = trim($request->noperiksa);

        $perik = DB::table('pemeriksaan')
                        ->join('pasien','pemeriksaan.user_id','=','pasien.user_id')
                        ->where('pemeriksaan.id',$id)
                        ->whereNull('pemeriksaan.deleted_at')
                        ->whereNull('pasien.deleted_at')
                        ->select('pemeriksaan.id','pasien.user_id','pasien.gender','pasien.nama','pasien.tempat_lahir','pasien.tanggal_lahir','pemeriksaan.status')->first();
        if ($perik->status!="OPEN") {
        	return response()->json("Status periksa ".$perik->status." ! ! !",422);	
        }else{
        	$data = array(
	            'noperiksa'=>$perik->id,
	            'norm'=>$perik->user_id,
	            'pasien'=>'('.$perik->gender.') '.$perik->nama,
	            'ttl'=>$perik->tempat_lahir.' '.date_format(date_create($perik->tanggal_lahir),'d-m-Y')
	        );

	        return response()->json(['data'=>$data],200);
        }

        

    }
    
}
