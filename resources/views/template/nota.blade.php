<style type="text/css">
@page{
    margin : 5 5 5 5;

}

/*@font-face {
    font-family: 'impact' !important;
    src: url({{ storage_path('fonts/impact.ttf') }}) format('truetype');
}*/

.br{
    line-height: 3px;
    margin-top: 5px;
}

</style>

<center>
    <h4>DENTAL CARE</h4>
    <table id="tb-head" width="100%">
       <!--  <tr>
            <td colspan="5"><center><h1>COBA</h1></center></td>
        </tr> -->

        <tr>
            <td width="20%"><label style="font-size: 12px;">ID. REG</label></td>
            <td>:</td>
            <td width="32%"><label style="font-size: 12px;">{{ $head->id }}</label></td>

            <td width="20%"><label style="font-size: 12px;">Tanggal</label></td>
            <td>:</td>
            <td width="32%"><label style="font-size: 12px;">{{ date_format(date_create($head->created_at),' d F Y')}}</label></td>
        </tr>
        <tr>
            <td><label style="font-size: 12px;">No. RM / NAMA</label></td>
            <td>:</td>
            <td><label style="font-size: 12px;">{{ $head->user_id }} / {{ $head->nama }}</label></td>

      

            <td><label style="font-size: 12px;">DOKTER / POLI</label></td>
            <td>:</td>
            <td><label style="font-size: 12px;">{{ $head->nama_dokter }} / {{ $head->nama_poli }}</label></td>
        </tr>
    </table>
    <hr>
    <table id="tb-det" width="100%" >
        <tr style="font-size:13px; font-weight: bold; text-align:center;">
            <td>#</td>
            <td>ID / PLU</td>
            <td>Item</td>
            <td>Qty</td>
            <td>Harga Satuan</td>
            <td>Sub. Total</td>
        </tr>
        <tr>
            <td colspan="6"><hr></td>
        </tr>
        <?php 
            $i = 1;
            $tot_qty = 0;
            foreach ($detl as $dt){
            $tot_qty = $tot_qty + $dt->qty;
        ?>
            <tr style="font-size:12px; text-align:center;">
                <td>{{$i++}}</td>
                <td>{{$dt->id}} / {{$dt->kode_product}}</td>
                <td>{{$dt->nama_product}}</td>
                <td>{{$dt->qty}}</td>
                <td>Rp. {{$dt->harga}}</td>
                <td>Rp. {{$dt->sub_total}}</td>
            </tr>
        <?php 
            } 
        ?>
        <tr>
            <td colspan="6"><hr></td>
        </tr>
        <tr style="font-size:13px; font-weight: bold; text-align: center; ">
            <td colspan="4"></td>
            <td>Total </td>
            <td>Rp {{$pemby->total}}</td>
        </tr>
        <tr>
            <td colspan="6"><span class="br"></span></td>

        </tr>
        <tr style="font-size:13px; font-weight: bold; text-align: center;">
            <td colspan="4"></td>
            <td>Bayar </td>
            <td>Rp {{$pemby->bayar}}</td>
        </tr>
        <tr>
            <td colspan="6"><span class="br"></span></td>
        </tr>
        <tr style="font-size:13px; font-weight: bold; text-align: center;">
            <td colspan="4"></td>
            <td>Kembali </td>
            <td>Rp {{$pemby->kembali}}</td>
        </tr>
    </table>
</center>


