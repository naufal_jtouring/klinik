<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use DB;
use Carbon\Carbon;
use DataTables;


use App\Models\Periksa;

class DashboardController extends Controller
{
    public function index(){
 		
        return view('dashboard');
    }

    public function ajaxGetPeriksa(){
        $now = Carbon::now()->format('Y-m-d');
        $data = DB::table('pemeriksaan')
                        ->join('poli','pemeriksaan.poli_id','=','poli.id')
                        ->join('pasien','pemeriksaan.user_id','=','pasien.user_id')
                        ->leftjoin('kelurahan','pasien.id_kel','=','kelurahan.id')
                        ->leftjoin('kecamatan','kelurahan.id_kec','=','kecamatan.id')
                        ->where(function ($query) use ($now){
                            $query->whereDate('pemeriksaan.created_at',$now)
                                    ->orWhere('pemeriksaan.status','OPEN');
                        })
                        ->whereNull('pemeriksaan.deleted_at')
                        ->whereNull('pasien.deleted_at')
                        ->orderby('pemeriksaan.status','desc')
                        ->orderby('pemeriksaan.created_at','desc')
                        ->select('pemeriksaan.id','pemeriksaan.status','pasien.user_id','pasien.nama','pasien.nik','pasien.gender','pasien.alamat','kelurahan.nama_kelurahan','kecamatan.nama_kecamatan','poli.nama_poli','pemeriksaan.poli_id');
       
        return DataTables::of($data)
                            ->editColumn('nama',function($data){
                                if ($data->gender=="LAKI-LAKI") {
                                    $gen = "(L) ";
                                }else{
                                    $gen = "(P) ";
                                }

                                return $gen.' '.$data->nama;
                            })
                            ->editColumn('poli_id',function($data){
                                return $data->nama_poli;
                            })
                            ->editColumn('status',function($data){
                                if ($data->status=="OPEN") {
                                    $selop = "selected";
                                    $selcl = "";
                                    $selcn = "";
                                    $disb = "";
                                }else if ($data->status=="CLOSE"){
                                    $selop = "";
                                    $selcl = "selected";
                                    $selcn = "";
                                    $disb = "disabled";
                                }else{
                                    $selop = "";
                                    $selcl = "";
                                    $selcn = "selected";
                                    $disb = "disabled";
                                }

                                return '<select class="form-control status" id="status" data-id ="'.$data->id.'" '.$disb.'>
                                            <option value="OPEN"'.$selop.'>OPEN</option>
                                            <option value="CLOSE"'.$selcl.'>CLOSE</option>
                                            <option value="CANCEL"'.$selcn.'>CANCEL</option>
                                        </select>';
                            })
                            ->editColumn('alamat',function($data){
                                return $data->alamat." Kel. ".$data->nama_kelurahan." Kec. ".$data->nama_kecamatan;
                            })
                            ->rawColumns(['nama','poli_id','status','alamat','status'])->make(true);
    }

    public function editStatus(Request $request){
        $id = $request->id;
        $val = $request->val;

        try {
            DB::begintransaction();
                Periksa::where('id',$id)->update(['status'=>$val]);
            DB::commit();
            $data_response = [
                            'status' => 200,
                            'output' => 'Edit status Sukses . . .'
                          ];
        } catch (Exception $ex) {
            DB::rollback();
            $message = $ex->getMessage();
            ErrorHandler::db($message);
            $data_response = [
                            'status' => 422,
                            'output' => 'Edit status Gagal ! ! !'
                          ];
        }

        return response()->json(['data'=>$data_response]);
    }
}
