<ul class="navigation navigation-main navigation-accordion">
	
	<!-- Main -->
	@permission(['menu-dashboard'])
	<li class="{{ $active == 'dashboard' ? 'active' : ''}}"><a href="{{ route('home.index') }}"><i class="icon-home4"></i> <span>Dashboard</span></a></li>
	@endpermission

	@permission(['menu-user-management'])
	<li class="navigation-header"><span>User Management</span> <i class="icon-menu"></i></li>
	<li class="{{ $active == 'user_account' ? 'active' : ''}}"><a href="{{ route('admin.user_account')}}"><i class="icon-theater"></i> <span>User Account</span></a></li>
	<li class="{{ $active == 'role_user' ? 'active' : ''}}"><a href="{{route('admin.formRole')}}"><i class="icon-menu2"></i> <span>Role</span></a></li>
	@endpermission

	@permission(['menu-master-data'])
	<li class="navigation-header"><span>Master Data</span> <i class="icon-menu"></i></li>\
	<li class="{{ $active == 'list_kec' ? 'active' : ''}}"><a href="{{ route('master.alamat.kecamatan') }}"><i class="icon-store"></i> <span>Kecamatan</span></a></li>
	<li class="{{ $active == 'list_kel' ? 'active' : ''}}"><a href="{{ route('master.alamat.kelurahan') }}"><i class="icon-store2"></i> <span>Kelurahan</span></a></li>
	<li class="{{ $active == 'list_poli' ? 'active' : ''}}"><a href="{{ route('master.dokter.poli') }}"><i class="icon-home8"></i> <span>Poli</span></a></li>
	<li class="{{ $active == 'list_dokter' ? 'active' : ''}}"><a href="{{ route('master.dokter.dokter') }}"><i class="icon-user-tie"></i> <span>Dokter</span></a></li>
	<li class="{{ $active == 'kategory_saatuan' ? 'active' : ''}}"><a href="{{ route('master.product.kategSatuan') }}"><i class="icon-balance"></i> <span>Kategory & Satuan</span></a></li>
	<li class="{{ $active == 'list_product' ? 'active' : ''}}"><a href="{{ route('master.product.product') }}"><i class="icon-price-tags"></i> <span>Product</span></a></li>
	
	@endpermission

	<!-- /page kits -->
	@permission(['menu-pendaftaran-pasien'])
	<li class="navigation-header"><span>Pendaftaran Pasien</span> <i class="icon-menu"></i></li>
	<li class="{{ $active == 'list_pasien' ? 'active' : ''}}"><a href="{{ route('pasien.listpasien') }}"><i class="icon-users4"></i> <span>Pasien</span></a></li>
	@endpermission

	@permission(['menu-pelayanan-pasien','menu-pemeriksaan-dokter'])
	<li class="navigation-header"><span>Pelayanan Pasien</span> <i class="icon-menu"></i></li>
	<li class="{{ $active == 'resep' ? 'active' : ''}}"><a href="{{ route('layanan.resep') }}"><i class="icon-lab"></i> <span>Apoteker</span></a></li>
	<li class="{{ $active == 'kasir' ? 'active' : ''}}"><a href="{{ route('layanan.kasir') }}"><i class="icon-cash2"></i> <span>Kasir</span></a></li>
	@permission(['menu-pemeriksaan-dokter'])
	<li class="{{ $active == 'periksa' ? 'active' : ''}}"><a href="{{ route('layanan.periksa') }}"><i class="fa fa-stethoscope"></i> <span>Pemeriksaan</span></a></li>
	@endpermission
	@endpermission

</ul>