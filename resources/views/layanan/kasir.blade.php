@extends('layouts.app', ['active' => 'kasir'])
@section('header')
<div class="page-header page-header-default">
	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="#"><i class="icon-home2 position-left"></i> Layanan</a></li>
			<li class="active">Kasir</li>
		</ul>
	</div>
</div>
@endsection

@section('content')
<div class="content">
	<div class="row">
		<div class=" panel panel-flat">
			<div class="panel-body">

				<div class="row form-group">
					<form action="{{ route('layanan.kasir.ajaxGetPeriksa') }}" id="form-search">
						<div class="col-lg-11">
							<label>Cari Nomor Periksa</label>
							<input type="text" name="noperiksa" class="form-control" id="noperiksa" required="">
						</div>
						<div class="col-lg-1">
							<button type="submit" id="cari" class="btn btn-primary" style="margin-top: 25px;"><span class=" icon-search4" ></span> Cari</button>
						</div>
					</form>
				</div>				
			</div>
		</div>
		<div class=" panel panel-flat hidden" id="panel-detail">
			<div class="panel-body">
				<div class="row form-group">
					<div class="col-lg-4">
						<label>NIK</label>
						<input type="text" name="txnik" id="txnik" class="form-control" readonly="">
					</div>
					<div class="col-lg-4">
						<label>Nama Pasien</label>
						<input type="text" name="txnama" id="txnama" class="form-control" readonly="">
					</div>
					<div class="col-lg-4">
						<label>Tempat Tanggal Lahir</label>
						<input type="text" name="txttl" id="txttl" class="form-control" readonly="">
					</div>
				</div>
				<div class="row form-group">
					<div class="col-lg-3">
						<label>No. Periksa</label>
						<input type="text" name="txnopr" id="txnopr" class="form-control" readonly="">
					</div>
					<div class="col-lg-3">
						<label>No. RM</label>
						<input type="text" name="txrm" id="txrm" class="form-control" readonly="">
					</div>
					<div class="col-lg-3">
						<label>Poli</label>
						<input type="text" name="txpoli" id="txpoli" class="form-control" readonly="">
					</div>
					<div class="col-lg-3">
						<label>Dokter</label>
						<input type="text" name="txdok" id="txdok" class="form-control" readonly="">
					</div>
				</div>
				<br>
				<div class="row form-group" id="sel-periksa">
					<div class="col-lg-12">
						<label><b>Jenis Pelayanan</b></label>
						<select id="add-periksa" class="form-control select-search">
							<option value="">--Pilih Bea Periksa--</option>
						</select>
					</div>
				</div>
				<br>
				<div class="row form-group">
					<div class="table-responsive">
						<table class ="table table-basic table-condensed" id="table-resep">
							<thead>
								<tr>
									<th>#</th>
									<th>ID Product / PLU</th>
									<th>Nama Product</th>
									<th>Qty</th>
									<th>Harga (satuan)</th>
									<th>Sub Total</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody></tbody>
							<tfoot>
								<tr>
									<td colspan="4"></td>
									<td><b>Total Resep</b></td>
									<td><label id="lb_totresep" style="font-weight: bold;"></label></td>
								</tr>
							</tfoot>
						</table>
					</div>
				</div>
				<br>
				<div class="row form-group">
					<div class="col-lg-3">
						<label>Total Biaya</label>
						<input type="text" name="total" id="total" class="form-control" readonly="">
					</div>
					<div class="col-lg-3">
						<label>Bayar</label>
						<input type="text" name="bayar" id="bayar" class="form-control" >
					</div>
					<div class="col-lg-3">
						<label>Kembali</label>
						<input type="text" name="kembali" id="kembali" class="form-control" readonly="">
					</div>
					<div class="col-lg-3">
						<center>
							<button class="btn btn-success" id="btn-save" style="margin-top: 25px;"><span class="icon-cart-add2"></span> Simpan</button>
							<button class="btn btn-warning hidden" id="btn-print" style="margin-top: 25px;"><span class="icon-printer4"></span> Print</button>
						</center>
					</div>
				</div>
				
				
			</div>
		</div>
	</div>
</div>

@endsection




@section('js')
<script type="text/javascript">
$(document).ready(function(){
	
	$('#form-search').submit(function(event){
		event.preventDefault();

		$.ajaxSetup({
	        headers: {
	            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	        }
	    });
	    $.ajax({
	        type: 'get',
	        url :  $('#form-search').attr('action'),
	        data : {noperiksa:$('#noperiksa').val()},
	        beforesend:function(){
	        	loading();
	        },
	        success: function(response) {
	        	var perik = response.periksa;
	        	
	        	if (perik.gender=='LAKI-LAKI') {
	        		var gend = '(L) ';
	        	}else{
	        		var gend = '(P) '
	        	}
	        	$('#txnik').val(perik.nik);
	        	$('#txnama').val(gend+perik.nama);
	        	$('#txttl').val(perik.tempat_lahir+' '+perik.tanggal_lahir);
	        	$('#txrm').val(perik.user_id);
	        	$('#txnopr').val(perik.id);
	        	$('#txpoli').val(perik.nama_poli);
	        	$('#txdok').val(perik.nama_dokter);

	        	var bayar = response.bayar;

	        	if (bayar!=null) {
	        		$('#bayar').val(bayar.bayar);
	        		$('#kembali').val(bayar.kembali);
	        		
	        		document.getElementById('bayar').readOnly = true;
	        		document.getElementById('kembali').readOnly = true;
	        		// document.getElementById('add-periksa').readOnly = true;
	        		$('#sel-periksa').addClass('hidden');
	        		$('#btn-save').addClass('hidden');
	        		$('#btn-print').removeClass('hidden');
	        	}

	        	detail(response.resep);
	        	listBea();
	        	$('#panel-detail').removeClass('hidden');
                 $.unblockUI();
	        },
	        error: function(response) {
	          
	        
                alert(422,"Nomor Periksa Tidak ditemukan");
	        }
	    });
	});

	$('#bayar').on('keyup',function(){
		var kemb  = $('#bayar').val() - $('#total').val();

		$('#kembali').val(kemb);
	});

	$('#btn-save').click(function(event){
		event.preventDefault();
			var bayar = $('#bayar').val();
			var total = $('#total').val();

		if (bayar < total) {
			alert(422,"Pembeyaran Kurang");
			return false;
		}

		$.ajaxSetup({
	        headers: {
	            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	        }
	    });
	    $.ajax({
	        type: 'post',
	        url :  "{{ route('layanan.kasir.addPembayaran') }}",
	        data : {idperiksa:$('#txnopr').val(),total:$('#total').val(),bayar:$('#bayar').val(),kembali:$('#kembali').val()},
	        beforesend:function(){
	        	loading();
	        },
	        success: function(response) {
	        	var notif = response.data;
                alert(notif.status,notif.output);
                 
                 print($('#txnopr').val());
                 $('#form-search').submit();
                 $.unblockUI();
                 // window.location.reload();
	        },
	        error: function(response) {
	          
	        	$.unblockUI();
                alert(response.status,response.responseText);
	        }
	    });
	});


	$('#add-periksa').on('change',function(){
		var id  = $('#add-periksa').val();

		if (id=="" || id==null) {
			alert(422,"Jenis Layanan Belum dipilih");
			return false;
		}

		$.ajaxSetup({
	        headers: {
	            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	        }
	    });
	    $.ajax({
	        type: 'post',
	        url :  "{{ route('layanan.kasir.addBeaPeriksa') }}",
	        data : {id:id,noperiksa:$('#txnopr').val()},
	        beforesend:function(){
	        	loading();
	        },
	        success: function(response) {
	        	var notif = response.data;
	            alert(notif.status,notif.output);
	            $('#form-search').submit();
	            $.unblockUI();
	            
	        },
	        error: function(response) {
	          
	        	$.unblockUI();
	            alert(response.status,response.responseText);
	        }
	    });
	});

	$('#btn-print').click(function(event){
		event.preventDefault();

		print($('#txnopr').val());
	});
	
});

function detail(arr){

	var totresp = 0;
	$('#table-resep > tbody').empty();
	for (var i = 0; i < arr.length; i++) {
		totresp = totresp + arr[i]['sub_total'];
		var no = i+1;
		$('#table-resep > tbody').append(
			"<tr><td>"+no+"</td><td>"+arr[i]['id']+" ("+arr[i]['kode_product']+")</td><td>"+arr[i]['nama_product']+"</td><td>"+arr[i]['qty']+" "+arr[i]['nama_satuan']+"</td><td>"+arr[i]['harga']+"</td><td>"+arr[i]['sub_total']+"</td><td><button class='btn btn-warning' data-id='"+arr[i]['listid']+"' onclick='delItem(this);'>DEL</button></td></tr>"
			);
	}

	$('#lb_totresep').text(totresp);
	$('#total').val(totresp);

}

function listBea(){
	$.ajax({
	    type: 'get',
	    url :  "{{ route('layanan.kasir.listBea') }}",

	    success: function(response) {
	    	var lb = response.listbea;

	    	$('#add-periksa').empty();
	    	$('#add-periksa').append('<option value="">--Pilih Jenis Layanan--</option>');
	    	for (var i = 0; i < lb.length; i++) {
	    		$('#add-periksa').append('<option value="'+lb[i]['id']+'&'+lb[i]['harga']+'">'+lb[i]['nama_product']+'</option>');
	    	}
	    },
	    error: function(response) {
	      console.log(response);
	    }
	});
}

function print(id){
	var link = "{{ route('layanan.kasir.printNota') }}";
	var url = link+'?id='+id;
	window.open(url);
}

function delItem(e){
	var id = e.getAttribute('data-id');
	
	$.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type: 'post',
        url :  "{{ route('layanan.kasir.deleteItem') }}",
        data : {id:id},
        beforesend:function(){
        	loading();
        },
        success: function(response) {
        	var notif = response.data;
            alert(notif.status,notif.output);
            $('#form-search').submit();
            $.unblockUI();
            
        },
        error: function(response) {
          
        	$.unblockUI();
            alert(response.status,response.responseText);
        }
    });
}





</script>
@endsection