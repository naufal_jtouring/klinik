<?php

namespace App\Http\Controllers\Layanan;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use DataTables;
use Auth;
use Carbon\Carbon;

use App\Models\DetailResep;
use App\Models\Periksa;
use App\Models\HasilPeriksa; 
use App\Models\Dokter; 

class PeriksaController extends Controller
{
    public function periksa(){
        return view('layanan.periksa');
    }

    public function ajaxPeriksa(Request $request){
        $noperiksa = $request->noperiksa;

        $data = DB::table('pemeriksaan')
                    ->join('pasien','pemeriksaan.user_id','=','pasien.user_id')
                    ->join('poli','pemeriksaan.poli_id','=','poli.id')
                    ->whereNull('pemeriksaan.deleted_at')
                    ->where('pemeriksaan.id',$noperiksa)
                    ->select('pasien.user_id','pasien.nama','pasien.nik','pasien.tanggal_lahir','pasien.gender','pemeriksaan.id','pemeriksaan.status','poli.nama_poli','pemeriksaan.status')
                    ->first();

        if(empty($data)){
             return response()->json("Nomor Periksa Tidak ditemukan",422);
        }else if($data->status!="OPEN"){
             return response()->json("Status periksa ".$data->status." ! ! !",422);    
        }else{
            $tlh = date_create($data->tanggal_lahir);
            $now = date_create(Carbon::now()->format('Y-m-d'));
            $diff = $now->diff($tlh);

           
            if ($data->gender=="LAKI-LAKI") {
                $gen = "(L)";
            }else{
                $gen = "(P)";
            }

            $pasien = array(
                'id'=>$data->id,
                'noperiksa'=>$data->id." (".$data->nama_poli.")",            
                'user_id'=>$data->user_id,
                'nama'=>$gen." ".$data->nama,
                'nik'=>$data->nik,
                'umur'=>$diff->y." Th. ".$diff->m." Bln. ".$diff->d." Hr.",
                'status'=>$data->status
            );


            return response()->json(['pasien'=>$pasien],200);
        }

        
    }

    public function ajaxRiwayat(Request $request){
        $norm = $request->norm;
      
        $data = DB::table('pemeriksaan')
                        ->join('poli','pemeriksaan.poli_id','=','poli.id')
                        ->Join('hasil_periksa','pemeriksaan.id','=','hasil_periksa.id_pemeriksaan')
                        ->where('pemeriksaan.user_id',$norm)
                        ->whereNull('pemeriksaan.deleted_at')
                        ->whereNull('hasil_periksa.deleted_at')
                        ->select('poli.nama_poli','hasil_periksa.created_at','hasil_periksa.pemeriksaan_fisik','hasil_periksa.diagnosis','hasil_periksa.tindakan','pemeriksaan.id')
                        ->orderby('hasil_periksa.created_at','desc');
        
        return DataTables::of($data)
                            ->addColumn('tanggal_periksa',function($data){
                                return date_format(date_create($data->created_at),'d-m-Y');
                            })
                            ->editColumn('id',function($data){
                                return $data->id." (".$data->nama_poli.")";
                            })
                            ->rawColumns(['tanggal_periksa','id'])->make(true);
    }

   

    public function addPeriksa(Request $request){
        $nopr = $request->tmperiksa;
        $pemr_fisik = trim($request->pemr_fisik);
        $diagnosis = trim($request->diagnosis);
        $tindakan = trim($request->tindakan);
        $status = $request->status;

        try {
            DB::begintransaction();
                $getdoc = Dokter::where('user_id',Auth::user()->id)->first();
                $detail = array(
                    'id_pemeriksaan'=>$nopr,
                    'pemeriksaan_fisik'=>$pemr_fisik,
                    'diagnosis'=>$diagnosis,
                    'tindakan'=>$tindakan,
                    'created_at'=>carbon::now()
                );

           
                HasilPeriksa::firstOrCreate($detail);

                Periksa::where('id',$nopr)->update(['status'=>$status,'dokter_id'=>$getdoc->id,'updated_at'=>Carbon::now()]);
            DB::commit();
            $data_response = [
                            'status' => 200,
                            'output' => 'Simpan data Sukses . . .'
                          ];
        } catch (Exception $ex) {
            DB::rollback();
            $message = $ex->getMessage();
            ErrorHandler::db($message);
            $data_response = [
                            'status' => 422,
                            'output' => 'Simpan data Gagal ! ! !'
                          ];
        }

        return response()->json(['data'=>$data_response]);
    }

    
}
