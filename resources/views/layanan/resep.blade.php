@extends('layouts.app', ['active' => 'resep'])
@section('header')
<div class="page-header page-header-default">
	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="#"><i class="icon-home2 position-left"></i> Layanan</a></li>
			<li class="active">Resep</li>
		</ul>
	</div>
</div>
@endsection

@section('content')
<div class="content">
	<div class="row">
		<div class=" panel panel-flat">
			<div class="panel-body">

				<div class="row form-group">
					<form action="{{ route('layanan.resep.ajaxGetPeriksa') }}" id="form-search">
						<div class="col-lg-11">
							<label>Cari Nomor Periksa</label>
							<input type="text" name="noperiksa" class="form-control" id="noperiksa" required="">
						</div>
						<div class="col-lg-1">
							<button type="submit" id="cari" class="btn btn-primary" style="margin-top: 25px;"><span class=" icon-search4" ></span> Cari</button>
						</div>
					</form>
				</div>
				<div class="row form-group">
					<div class="col-lg-3">
						<label>No. Periksa</label>
						<input type="text" name="txidpr" id="txidpr" class="form-control" readonly="">
					</div>
					<div class="col-lg-3">
						<label>No. RM</label>
						<input type="text" name="txrm" id="txrm" class="form-control" readonly="">
					</div>
					<div class="col-lg-3">
						<label>Nama Pasien</label>
						<input type="text" name="txnama" id="txnama" class="form-control" readonly="">
					</div>
					<div class="col-lg-3">
						<label>Tempat Tanggal Lahir</label>
						<input type="text" name="txttl" id="txttl" class="form-control" readonly="">
					</div>
				</div>
				<hr>
				<div class="row form-group">
					<div class="col-lg-5">
						<label>Product</label>
						<select id="produk" class="form-control select-search" required="" onchange="focus(this);">
							<option value="null">--Pilih Produk--</option>
							@foreach($prod as $pr)
								<option value="{{$pr->id}}">{{$pr->nama_product}}</option>
							@endforeach
						</select>
					</div>
					<div class="col-lg-3">
						<label>QTY</label>
						<input type="number" name="qty" id="qty" class="form-control" required="" value="1">
					</div>
					<div class="col-lg-2">
						<button class="btn bg-teal-400" id="btn-add" style="margin-top: 30px;" disabled=""><span class="icon-add"></span> Tambah</button>
					</div>
					<div class="col-lg-2">
						<button class="btn btn-success" id="btn-save" style="margin-top: 30px;" disabled=""><span class="icon-folder-download"></span> Simpan</button>
					</div>
				</div>
				<hr>
				<div class="row form-group">
					<div class="table-responsive">
						<table class ="table table-basic table-condensed" id="table-list">
							<thead>
								<tr>
									<th>#</th>
									<th>ID Produk</th>
									<th>Nama</th>
									<th>Qty</th>
									<th>Harga</th>
									<th>Total</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody></tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection


@section('js')
<script type="text/javascript">
$(document).ready(function(){
	var resep = [];
	var total = 0;

	$('#form-search').submit(function(e){
		e.preventDefault();

		$.ajaxSetup({
	        headers: {
	            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	        }
	    });
	    $.ajax({
	        type: 'get',
	        url :  $('#form-search').attr('action'),
	        data:{noperiksa:$('#noperiksa').val()},
	        beforeSend : function(){
	        	loading();
	        },
	        success: function(response) {

	        	var data = response.data;
	        	$('#txidpr').val(data.noperiksa);
	        	$('#txnama').val(data.pasien);
	        	$('#txrm').val(data.norm);
	        	$('#txttl').val(data.ttl);
	        	document.getElementById('btn-add').disabled = false;
	        	$.unblockUI();
	        },
	        error: function(response) {
	        	
	           alert(response.status,response.responseText);
	           $.unblockUI();
                
	        }
	    });
	});

	$('#btn-add').click(function(event){
		event.preventDefault();

		var idprod = $('#produk').val();
		var qty = $('#qty').val();

	
		if (idprod=='null') {
			alert(422,"Pilih Product terlebih dahulu ! ! !");
			return true;
		}else if (qty==0) {
			alert(422,"Isi qty product terlebih dahulu");
			return true;
		}else{
			$.ajaxSetup({
		        headers: {
		            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		        }
		    });
		    $.ajax({
		        type: 'get',
		        url :  "{{ route('layanan.resep.getProduct') }}",
		        data:{id:idprod},
		        beforeSend : function(){
		        	loading();
		        },
		        success: function(response) {
		        	var prod = response.prod;

		        	resep.push({
		        		'id_prod':prod.id,
		        		'kode_product':prod.kode_product,
		        		'nama':prod.nama_product,
		        		'qty':qty,
		        		'satuan':prod.nama_satuan,
		        		'harga':prod.harga,
		        		'sub_tot':prod.harga * qty
		        	});

		        	total = total + (prod.harga * qty);

		        	setTable(resep);
		        	$('#txtotal').val(total);
		        	$('#produk').val('null').trigger('change');
		        	$('#qty').val(1);
		        	document.getElementById('btn-save').disabled = false;
		        	$.unblockUI();
		         	
		        },
		        error: function(response) {
		           // var notif = response.data;
	            //     alert(notif.status,notif.output);
	            		$.unblockUI();
			           	alert(response.status,response.responseText);
	                
		        }
		    });
		}


		
	});
	

	

	
	$('#table-list').on('click','.delItem',function(){
		var idx = $(this).data('id');
			var subtot = resep[idx]['sub_tot'];
			total = total - subtot;
			$('#txtotal').val(total);
			resep.splice(idx,1);
			$('#txkembali').val(0);
			$('#txbayar').val(0);
			setTable(resep);

	});



	$("#btn-save").click(function(e){
		e.preventDefault();

		$.ajaxSetup({
	        headers: {
	            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	        }
	    });
	    $.ajax({
	        type: 'post',
	        url :  "{{ route('layanan.resep.addTransaksi') }}",
	        data:{resep:resep,noperiksa:$('#txidpr').val()},
	        beforeSend : function(){
	        	loading();
	        },
	        success: function(response) {
	        	var notif = response.data;
                alert(notif.status,notif.output);
	         	$.unblockUI();
	         	 window.location.reload();
	        },
	        error: function(response) {

            		$.unblockUI();
		           	alert(response.status,response.responseText);
                
	        }
	    });

	});
	

	

	
});

function setTable(array){
	$('#table-list > tbody').empty();
	for (var i = 0; i < array.length; i++) {
		var no = i+1;
		$('#table-list').append('<tr><td>'+no+'</td><td>'+array[i]['kode_product']+'</td><td>'+array[i]['nama']+'</td><td>'+array[i]['qty'] +' ('+array[i]['satuan']+')</td><td>'+array[i]['harga']+'</td><td>'+array[i]['sub_tot']+'</td><td><button class="btn btn-warning delItem" data-id="'+i+'" >Del</button></td></tr>');

	}
}

function focus(e){
	$('#qty').focus();
}







</script>
@endsection