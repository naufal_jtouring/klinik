@extends('layouts.app', ['active' => 'periksa'])
@section('header')
<div class="page-header page-header-default">
	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="#"><i class="icon-home2 position-left"></i> Layanan</a></li>
			<li class="active">Pemeriksaan</li>
		</ul>
	</div>
</div>
@endsection

@section('content')
<div class="content">
	<div class="row">
		<div class=" panel panel-flat"> 
			<div class="panel-body">
				<div class="row form-group">
					<form action="{{ route('layanan.periksa.ajaxPeriksa') }}" id="form-search">
						<div class="col-lg-11">
							<label><b>Nomor Periksa</b></label>
							<input type="text" name="noperiksa" id="noperiksa" class="form-control" required="" placeholder="Nomor Periksa">
						</div>
						<div class="col-lg-1">
							<button class="btn btn-primary" type="submit" style="margin-top:25px;"><span class="icon-search4"></span> CARI</button>
						</div>
					</form>
					
				</div>
				<div class="row">
					<div class="col-lg-3">
						<label>No. Periksa</label>
						<input type="text" name="txperiksa" id="txperiksa" class="form-control" readonly="">
						<input type="text" name="txid" id="txid" class="form-control hidden">
					</div>
					<div class="col-lg-3">
						<label>No. RM</label>
						<input type="text" name="txrm" id="txrm" class="form-control" readonly="">
					</div>
					<div class="col-lg-3">
						<label>Nama</label>
						<input type="text" name="txnama" id="txnama" class="form-control" readonly="">
					</div>
					<div class="col-lg-3">
						<label>Umur</label>
						<input type="text" name="txumur" id="txumur" class="form-control" readonly="">
						
					</div>
				</div>
				
			</div>
		</div>
	</div>
	<div class="row">
		<div class=" panel panel-flat">
			<div class="panel-body">
				<div class="row">
					<button class="btn btn-default" disabled="" id="btn-add"><span class="icon-plus2"></span> Tambah</button>
				</div>
				<div class="row">
					<div class="table-responsive">
						<table class ="table table-basic table-condensed" id="table-list">
							<thead>
								<th>#</th>
								<th>Tanggal</th>
								<th>No. Periksa</th>
								<th>Pemeriksaan Fisik</th>
								<th>Diagnosis</th>
								<th>Tindakan</th>
							</thead>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection

@section('modal')
<div id="modal_add" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h5 class="modal-title">Hasil Pemeriksaan</h5>
			</div>

			<div class="modal-body">
				<form action="{{ route('layanan.periksa.addPeriksa') }}" id="form-add">
					@csrf
					<div class="row">
						<label><b>Pemeriksaan Fisik</b></label>
						<textarea id="pemr_fisik" class="form-control" style="height: 100px;"></textarea>
					</div>	
					<div class="row">
						<label><b>Diagnosis</b></label>
						<textarea id="diagnosis" class="form-control" style="height: 100px;"></textarea>
					</div>		
					<div class="row">
						<label><b>Tindakan</b></label>
						<textarea id="tindakan" class="form-control" style="height: 100px;"></textarea>
						<input type="text" name="tmperiksa" id="tmperiksa" class="hidden">
					</div>	
					<div class="row">
						<button type="submit" class="btn btn-success"><span></span> Simpan</button>
					</div>
					
				</form>
			</div>
		</div>
	</div>
</div>
@endsection
@section('js')
<script type="text/javascript">
$(document).ready(function(){
	

	$('#form-search').submit(function(event){
		event.preventDefault();

		$.ajaxSetup({
	        headers: {
	            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	        }
	    });
	    $.ajax({
	        type: 'get',
	        url :  $('#form-search').attr('action'),
	        data:{noperiksa:$('#noperiksa').val()},
	        beforeSend : function(){
	        	loading();
	        },
	        success: function(response) {
	        	var pasn = response.pasien;

	        	$('#txid').val(pasn.id);
	        	$('#txperiksa').val(pasn.noperiksa);
	        	$('#txrm').val(pasn.user_id);
	        	$('#txnama').val(pasn.nama);
	        	$('#txumur').val(pasn.umur);
	        	setRiwayat(pasn.user_id);
	        	if (pasn.status=="OPEN") {
	        		document.getElementById('btn-add').disabled =false;
	        	}else{
	        		document.getElementById('btn-add').disabled =true;
	        	}
	        	
	        	
	          	$.unblockUI();

	        },
	        error: function(response) {
	           
            		$.unblockUI();
		           	alert(response.status,response.responseText);
                
	        }
	    });
	});

	$('#btn-add').click(function(){
		$('#pemr_fisik').val('');
		$('#diagnosis').val('');
		$('#tindakan').val('');
		$('#tmperiksa').val($('#txid').val());
		
		$('#modal_add').modal('show');
	});

	$('#form-add').submit(function(event){
		event.preventDefault();

		$.ajaxSetup({
	        headers: {
	            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	        }
	    });
	    $.ajax({
	        type: 'post',
	        url :  $('#form-add').attr('action'),
	        data:{tmperiksa:$('#tmperiksa').val(),pemr_fisik:$('#pemr_fisik').val(),diagnosis:$('#diagnosis').val(),tindakan:$('#tindakan').val()},
	        beforeSend : function(){
	        	loading();
	        },
	        success: function(response) {
	        	
	        	var notif = response.data;
                alert(notif.status,notif.output);
                
                $('#modal_add').modal('hide');
             	$('#form-search').submit();
                $.unblockUI();
	        },
	        error: function(response) {
	           
            		$.unblockUI();
		           	alert(response.status,response.responseText);
                
	        }
	    });
	
	});
});

function setRiwayat(norm){
	var table = $('#table-list').DataTable({
		processing:true,
		serverSide:true,
		deferRender:true,
		dom:'<"datatable-header"fBl><t><"datatable-footer"ip>',
		language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        },
        ajax: {
            type: 'GET',
            url: "{{ route('layanan.periksa.ajaxRiwayat') }}",
            data : {norm:norm}
        },
        fnCreatedRow: function (row, data, index) {
            var info = table.page.info();
            var value = index+1+info.start;
            $('td', row).eq(0).html(value);
        },
        columns: [
            {data: null, sortable: false, orderable: false, searchable: false},
            {data: 'tanggal_periksa', name: 'tanggal_periksa'},
            {data: 'id', name: 'id'},
            {data: 'pemeriksaan_fisik', name: 'pemeriksaan_fisik', sortable: false, orderable: false, searchable: false},
            {data: 'diagnosis', name: 'diagnosis', sortable: false, orderable: false, searchable: false},
            {data: 'tindakan', name: 'tindakan', sortable: false, orderable: false, searchable: false}
        ]
	});
}


</script>
@endsection

