@extends('layouts.app', ['active' => 'dashboard'])
@section('header')
<div class="page-header page-header-default">
	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="index.html"><i class="icon-home2 position-left"></i> Home</a></li>
			<li class="active">Dashboard</li>
		</ul>
	</div>

</div>
@endsection

@section('content')
<div class="content">
	<!-- Main charts -->
	<div class="panel panel-flat">
		<div class="panel-heading">
			<div class="row">
				<div class="col-lg-8">
					<h2><span class=" icon-stats-bars4"></span> Daftar Pasien Periksa</h2>
				</div>
			</div>
		</div>
		<div class="panel-body">
			<div class="row">
				<div class="table-responsive">
					<table class ="table table-basic table-condensed" id="table-list">
						<thead>
							<th>#</th>
							<th>No. Periksa</th>
							<th>No. RM</th>
							<th>Nama</th>
							<th>Poli</th>
							<th>Status</th>
							<th>Alamat</th>
						</thead>
					</table>
				</div>
			</div>
		</div>
	</div>

</div>
@endsection

@section('js')
<script type="text/javascript">
$(document).ready(function(){

	var table = $('#table-list').DataTable({
		processing:true,
		serverSide:true,
		deferRender:true,
		dom:'<"datatable-header"fBl><t><"datatable-footer"ip>',
		language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        },
        ajax: {
            type: 'GET',
            url: "{{ route('home.ajaxGetPeriksa') }}"
        },
        fnCreatedRow: function (row, data, index) {
            var info = table.page.info();
            var value = index+1+info.start;
            $('td', row).eq(0).html(value);
        },
        columns: [
            {data: null, sortable: false, orderable: false, searchable: false},
            {data: 'id', name: 'id'},
            {data: 'user_id', name: 'user_id'},
            {data: 'nama', name: 'nama'},
            {data: 'poli_id', name: 'poli_id'},
            {data: 'status', name: 'status'},
            {data: 'alamat', name: 'alamat'}
        ]
	});

});
</script>
@endsection