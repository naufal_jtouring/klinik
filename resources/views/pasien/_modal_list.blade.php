
<div id="modal_rm" class="modal fade">
	<div class="modal-dialog modal-full">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h5 class="modal-title">Riwayat Pasien</h5>
			</div>

			<div class="modal-body">
				<div class="table-responsive">
					<table class ="table table-basic table-condensed" id="table-rm">
						<thead>
							<tr>
								<th>#</th>
								<th>ID Priksa</th>
								<th>Status</th>
								<th>Hasil Periksa</th>
							</tr>
						</thead>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>