
<div id="modal_daftar" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h5 class="modal-title">Pendaftaran Periksa</h5>
			</div>

			<div class="modal-body">
				<form action="{{ route('regist.addPendaftaran') }}" id="form-daftar">
					<div class="row">
						<label><b>Nama Pasien</b></label>
						<select class="form-control select-search" id="norm">
							
						</select>
					</div>
					<br>
					<div class="row">
						<div class="col-lg-4">
							<label><b>No. RM</b></label>
							<input type="text" name="txrm" id="txrm" class="form-control" readonly="">
						</div>
						<div class="col-lg-4">
							<label><b>Tempat Tanggal Lahir</b></label>
							<input type="text" name="txttl" id="txttl" class="form-control" readonly="">
						</div>
						<div class="col-lg-4">
							<label><b>Gender</b></label>
							<input type="text" name="txgender" id="txgender" class="form-control" readonly="">
						</div>
					</div>
					<div class="row">
						<div class="col-lg-12">
							<label><b>Alamat</b></label>
							<input type="text" name="txalamat" id="txalamat" class="form-control" readonly="">
						</div>
						
					</div>
					<br>
					<div class="row">
						<label><b>Poli</b></label>
						<select class="form-control select" id="poli">
							
						</select>
					</div>
					<div class="row">
						<button class="btn btn-primary" type="submit">SIMPAN</button>
					</div>
				</form>
				
			</div>
		</div>
	</div>
</div>

