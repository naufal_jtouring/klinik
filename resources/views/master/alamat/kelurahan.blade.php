@extends('layouts.app', ['active' => 'list_kel'])
@section('header')
<div class="page-header page-header-default">
	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="#"><i class="icon-home2 position-left"></i> Master</a></li>
			<li class="active">Kelurahan</li>
		</ul>
	</div>
</div>
@endsection

@section('content')
<div class="content">
	<div class="row">
		<div class=" panel panel-flat">
			<div class="panel-body">
				<div class="row form-group">
					<div class="col-lg-3">
						<label>ID Kelurahan</label>
						<input type="text"  name="id_kel" id="id_kel" value="{{$idkel}}" readonly="" class="form-control">
					</div>
					<div class="col-lg-3">
						<label>Nama Kelurahan</label>
						<input type="text" name="nm_kel" id="nm_kel" class="form-control" placeholder="Nama Kelurahan" required="">
					</div>
					<div class="col-lg-3">
						<label>Kecamatan</label>
						<select id="idkec" class="form-control select" required="">
							<option value="">--Pilih Kecamatan--</option>

							@foreach($kec as $kc)
								<option value="{{$kc->id}}">{{ $kc->nama_kecamatan }}</option>
							@endforeach
						</select>
					</div>
					<div class="col-lg-2">
						<button class="btn btn-primary" id="btn-save" style="margin-top: 30px;">Simpan</button>
					</div>
				</div>
				<div class="row form-group">
					<div class="table-responsive">
						<table class ="table table-basic table-condensed" id="table-list">
							<thead>
								<tr>
									<th>#</th>
									<th>ID Kel.</th>
									<th>Nama Kelurahan</th>
									<th>Nama Kecamatan</th>
								</tr>
							</thead>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection


@section('js')
<script type="text/javascript">
$(document).ready(function(){
	

	

	var table = $('#table-list').DataTable({
		processing:true,
		serverSide:true,
		deferRender:true,
		dom:'<"datatable-header"fBl><t><"datatable-footer"ip>',
		language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        },
        ajax: {
            type: 'GET',
            url: "{{ route('master.alamat.ajaxGetKel') }}"
        },
        fnCreatedRow: function (row, data, index) {
            var info = table.page.info();
            var value = index+1+info.start;
            $('td', row).eq(0).html(value);
        },
        columns: [
            {data: null, sortable: false, orderable: false, searchable: false},
            {data: 'id', name: 'id'},
            {data: 'nama_kelurahan', name: 'nama_kelurahan'},
            {data: 'id_kec', name: 'id_kec'}
        ]
	});

	table.on('preDraw', function() {
		loading();
	    Pace.start();
	})
	.on('draw.dt', function() {
	    $.unblockUI();
	    Pace.stop();
	});

	


	$('#btn-save').click(function(event){
		event.preventDefault();

		$.ajaxSetup({
	        headers: {
	            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	        }
	    });
	    $.ajax({
	        type: 'post',
	        url :  "{{ route('master.alamat.addKel')}}",
	        data:{id:$('#id_kel').val(),nm_kel:$('#nm_kel').val(),id_kec:$('#idkec').val()},
	        beforeSend : function(){
	        	loading();
	        },
	        success: function(response) {

	         	var notif = response.data;
                alert(notif.status,notif.output);
    
	            
	            window.location.reload();
	        },
	        error: function(response) {
	           // var notif = response.data;
            //     alert(notif.status,notif.output);
            		$.unblockUI();
		           	alert(response.status,response.responseText);
                
	        }
	    });
	});
	

	

	

	

	

	
});





</script>
@endsection