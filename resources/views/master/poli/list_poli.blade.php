@extends('layouts.app', ['active' => 'list_poli'])
@section('header')
<div class="page-header page-header-default">
	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="#"><i class="icon-home2 position-left"></i> Master</a></li>
			<li class="active">Poli</li>
		</ul>
	</div>
</div>
@endsection

@section('content')
<div class="content">
	<div class="row">
		<div class=" panel panel-flat">
			<div class="panel-body">
				<div class="row">
					<div class="col-lg-3">
						<button id="btn-add" class="btn btn-default"><span class="icon-user-plus"></span> Tambah</button>
					</div>
				</div>
				<div class="row form-group">
					<div class="table-responsive">
						<table class ="table table-basic table-condensed" id="table-list">
							<thead>
								<tr>
									<th>#</th>
									<th>ID Poli</th>
									<th>Nama Poli</th>
								</tr>
							</thead>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection

@section('modal')
<div id="modal_add" class="modal fade">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h5 class="modal-title">Poli</h5>
			</div>

			<div class="modal-body">
				<form action="{{ route('master.dokter.addPoli') }}" id="form-add">
					@csrf
					<div class="row">
						<div class="col-lg-6">
							<label class="display-block text-semibold">Nama Poli</label>
							<input type="text" name="txpoli" id="txpoli" class="form-control" required="" style="text-transform: uppercase;" placeholder="Nama Poli">
						</div>
						<div class="col-lg-6">
							<button class="btn btn-primary" id="btn-save" style="margin-top: 30px;"><span class="icon-file-download2"></span> Simpan</button>
						</div>
					</div>					
				</form>
			</div>
		</div>
	</div>
</div>


@endsection

@section('js')
<script type="text/javascript">
$(document).ready(function(){
	

	

	var table = $('#table-list').DataTable({
		processing:true,
		serverSide:true,
		deferRender:true,
		dom:'<"datatable-header"fBl><t><"datatable-footer"ip>',
		language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        },
        ajax: {
            type: 'GET',
            url: "{{ route('master.dokter.ajaxListPoli') }}"
        },
        fnCreatedRow: function (row, data, index) {
            var info = table.page.info();
            var value = index+1+info.start;
            $('td', row).eq(0).html(value);
        },
        columns: [
            {data: null, sortable: false, orderable: false, searchable: false},
            {data: 'id', name: 'id'},
            {data: 'nama_poli', name: 'nama_poli'}
        ]
	});

	table.on('preDraw', function() {
		loading();
	    Pace.start();
	})
	.on('draw.dt', function() {
	    $.unblockUI();
	    Pace.stop();
	});

	$(window).on('load',function(){
		
		table.clear();
		table.draw();
	
	});

	$('#btn-add').click(function(){
		$('#txpoli').val('');
		$('#modal_add').modal('show');
	});

	$('#form-add').submit(function(event){
		event.preventDefault();

		$.ajaxSetup({
	        headers: {
	            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	        }
	    });
	    $.ajax({
	        type: 'post',
	        url :  $('#form-add').attr('action'),
	        data:{nama:$('#txpoli').val()},
	        beforeSend : function(){
	        	loading();
	        },
	        success: function(response) {

	         	var notif = response.data;
                alert(notif.status,notif.output);
    
	            
	            table.clear();
	            table.draw();
	            $.unblockUI();
	            $('#txpoli').val('');
				$('#modal_add').modal('hide');
	          
	        },
	        error: function(response) {
	           
            		$.unblockUI();
		           	alert(response.status,response.responseText);
                
	        }
	    });
	});

	

	

	
});





</script>
@endsection