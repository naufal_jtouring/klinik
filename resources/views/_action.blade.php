<ul class="icons-list">
	<li class="dropdown">
		<a href="#" class="dropdown-toggle" data-toggle="dropdown">
			<i class="icon-menu9"></i>
		</a>

		<ul class="dropdown-menu dropdown-menu-right">
			
			@if(isset($edituser))
			<li><a href="{!! $edituser !!}"><i class="icon-paragraph-justify2"></i> Edit User</a></li>
			@endif

			@if(isset($innactive))
			<li><a href="{!! $innactive !!}"><i class="icon-x"  class="ignore-click innactive"></i> Innactive User</a></li>
			@endif

			@if(isset($editrole))
			<li><a href="{!! $editrole !!}"><i class="icon-paragraph-justify2"  class="ignore-click"></i> Edit Role</a></li>
			@endif

			@if(isset($editpasien))
			<li><a href="#" data-id="{!! $editpasien !!}" class=" ignore-click editpasien"><i class="icon-pencil3"></i> Edit Pasien</a></li>
			@endif

			@if(isset($regist))
			<li><a href="#" data-id="{!! $regist !!}" class=" ignore-click regist"><i class=" icon-quill4"></i> Registrasi Periksa</a></li>
			@endif

			@if(isset($riwayat))
			<li><a href="#" data-id="{!! $riwayat !!}" class=" ignore-click riwayat"><i class="icon-file-text3"></i> Riwayat Periksa</a></li>
			@endif

			@if(isset($edProduct))
			<li><a href="#" data-id="{!! $edProduct !!}" class=" ignore-click edProduct"><i class="icon-pencil3"></i> Edit</a></li>
			@endif

			@if(isset($delProduct))
			<li><a href="#" data-id="{!! $delProduct !!}" class=" ignore-click delProduct"><i class="icon-trash-alt"></i> Delete</a></li>
			@endif

			@if(isset($listrm))
			<li><a href="#" data-id="{!! $listrm !!}" class=" ignore-click listrm" onclick="viewList(this);"><i class="icon-paragraph-justify2"></i> List RM</a></li>
			@endif

		</ul>
	</li>
</ul>